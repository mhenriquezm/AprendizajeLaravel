<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PrimerController@index');
Route::get('/crear', 'PrimerController@create');
Route::get('articulos', 'PrimerController@store');
Route::get('/mostrar', 'PrimerController@show');