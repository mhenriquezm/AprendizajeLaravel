<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//php artisan route:list -> comando para ver todas las rutas y sus tipos

/*Al tener solo el slash / indica que a continuación del dominio no hay nada
	1. Utilizamos la clase Route y el método estático get
	2. El método get recibe dos parámetros, la url y una función anónima
	3. La función anónima retorna la vista que se mostrará en la url
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/pruebas', function () {
    return 'Esta es la página que habla sobre nosotros';
});

/* ********************* Rutas con parámetros ********************* */

/*Para agregar parámetros a una ruta se coloca en la url separadas por /
entre llaves de coloca el nombre, en caso de que el parámetro pueda ser nulo
se le coloca como sufijo el simbolo ? y pueden tener valores por default*/
Route::get('/post/{id}/{nombre?}', function ($id, $nombre = '') {
	$mensaje = 'Este es el post #' . $id;

	if($nombre != '') {
		$mensaje .= '. Creado por: ' . $nombre;
		return $mensaje;
	}

    return $mensaje;
})->where('nombre', '[a-zA-Z\'\s]+');

/* ********************* Rutas de controladores ********************* */

/*Especificamos la url, luego el nombre del controlador @ el metodo a utilizar, podemos pasar parámetros de ser necesario*/
//Route::get('/inicio/{id}', 'EjemploIIIController@index');

Route::get('/', 'PaginasController@inicio');
Route::get('/inicio', 'PaginasController@inicio');
Route::get('/quienesSomos', 'PaginasController@quienesSomos');
Route::get('/dondeEstamos', 'PaginasController@dondeEstamos');
Route::get('/foro', 'PaginasController@foro');

/* ********************* Controladores con recursos ********************* */

/*Utilizando el metodo resource creamos todas las rutas pertinentes al controlador de recursos, que nos ayudaran con las operaciones CRUD*/
Route::resource('posts', 'EjemploIIIController');