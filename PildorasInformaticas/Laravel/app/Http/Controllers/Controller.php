<?php

//Espacio de nombres similar a los paquetes de Java
namespace App\Http\Controllers;

//use importa clases, metodos o variables que estan en bibliotecas o namespaces
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

//Controlador base de Laravel, todos los controladores deben heredar de el
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
