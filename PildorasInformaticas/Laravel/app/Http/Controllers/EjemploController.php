<?php

namespace App\Http\Controllers;
/*Para nombrar un fichero de tipo controlador la nomenclatura es dar el nombre
seguido de la palabra controller usando camelcase EjemploController.php la clase controlador debe llamarse igual que el fichero*/
class EjemploController extends Controller {
	
	/*Controlador que enruta la app. En aplicaciones sencillas se pueden gestionar las rutas utilizando el fichero routes, pero ya para aplicaciones mas complejas que tengan muchas páginas, es más recomendable utilizar los controladores para realizar estas tareas*/
	public function inicio() {
		return 'Estás en el inicio del sitio';
	}
}

//La etiqueta php no se cierra en estos ficheros