<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Definiendo una ruta: usamos get para recibir el recurso definimos dos
parametros. 
	1. URL
	2. Funcion anonima para gestionar el evento
*/
Route::get('/primeraRuta', function() {
	return 'Hola mundo';
});

/*Ruta con parametros, ademas de parametros podemos especificar valores por defecto asignando un valor en la zona de parametros y dicho parametros asignarle el caracter comodin ?*/
Route::get('/nombre/{nombre}/apellido/{apellido?}', function($nombre, $apellido = null) {
	return 'Bienvenido ' . $nombre . ' ' . $apellido;
});

//Enrutado del controlador y acceso a su metodo index
Route::get('controlador', 'PrimerControlador@index');

//Enrutado del controlador de tipo resource
Route::resource('trainers', 'TrainerController');