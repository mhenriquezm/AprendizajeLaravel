@extends('layouts.app')

@section('title', 'Trainers create')

@section('contenido')
	{!! Form::open(['route' => 'trainers.store', 'method' => 'POST', 'files' => true, 'class' => 'form-group']) !!}
		<div class="form-group">
			{!! Form::label('nombre', 'Nombre') !!}
			{!! Form::text('nombre', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('avatar', 'Avatar') !!}
			{!! Form::file('avatar', ['class' => 'form-control-file']) !!}
		</div>

		{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
	{!! Form::close() !!}
@endsection