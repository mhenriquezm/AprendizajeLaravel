@extends('layouts.app')

@section('title', 'Trainers update')

@section('contenido')
	<img src="/img/{{$trainer->avatar}}" alt="{{$trainer->nombre}}" class="card-img-top rounded-circle mx-auto d-block" style="width: 200px; height: 200px; background-color: #efefef; margin: 20px;">

	{!! Form::model($trainer, ['route' => ['trainers.update', $trainer], 'method' => 'PUT', 'files' => true, 'class' => 'form-group']) !!}
		<div class="form-group">
			{!! Form::label('nombre', 'Nombre') !!}
			{!! Form::text('nombre', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('avatar', 'Avatar') !!}
			{!! Form::file('avatar', ['class' => 'form-control-file']) !!}
		</div>

		{!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
	{!! Form::close() !!}
@endsection