@extends('layouts.app')

@section('title', 'Trainers')

@section('contenido')
	<div class="row">
	@foreach($trainers as $trainer)
		<div class="col-sm">
			<div class="card text-center" style="width: 18rem; margin-top: 70px;">
				
				<img src="/img/{{$trainer->avatar}}" alt="{{$trainer->nombre}}" class="card-img-top rounded-circle mx-auto d-block" style="width: 100px; height: 100px; background-color: #efefef; margin: 20px;">

				<div class="card-body">
					<h5 class="card-title">{{$trainer->nombre}}</h5>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					<a href="/trainers/{{$trainer->slug}}" class="btn btn-primary">Ver más</a>
				</div>
			</div>
		</div>
	@endforeach
	</div>
@endsection