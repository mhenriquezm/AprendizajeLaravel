@extends('layouts.app')

@section('title', 'Trainer')

@section('contenido')
	<img src="/img/{{$trainer->avatar}}" alt="{{$trainer->nombre}}" class="card-img-top rounded-circle mx-auto d-block" style="width: 200px; height: 200px; background-color: #efefef; margin: 20px;">

	<div class="text-center">
		<h5>{{$trainer->nombre}}</h5>
		<p>
			Some quick example text to build on the card title and make up the bulk of the card's content. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat dignissimos quod harum officiis dolorem eius! Voluptates repellendus provident, quos eos dolore assumenda voluptatibus atque, officiis doloribus hic, tempora dolorem maiores!
		</p>
		<a href="/trainers/{{$trainer->slug}}/edit" class="btn btn-primary">Editar</a>
	</div>
@endsection