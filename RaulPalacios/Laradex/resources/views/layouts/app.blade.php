<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="UTF-8">

	<title>LaraDex - @yield('title')</title>

	<link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/all.css')}}">
	<link rel="stylesheet" href="{{asset('assets/fonts/Nunito.css')}}">
	<link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap-grid.css')}}">
	<link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap-reboot.css')}}">

	@yield('estilosInternos')
	@yield('scriptsInternos')
</head>
<body>
	<nav class="navbar navbar-dark bg-primary">
		<a href="#" class="navbar-brand">LaraDex</a>
	</nav>
	<div class="container">
		@yield('contenido')
	</div>

	<script src="{{asset('assets/js/sortTable.js')}}"></script>
	<script src="{{asset('assets/js/filter.js') }}"></script>
	<script src="{{asset('assets/js/functions.js')}}"></script>
	<script src="{{asset('assets/jquery/jquery-3.3.1.slim.min.js')}}"></script>
	<script src="{{asset('assets/popper/popper.min.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.bundle.js')}}"></script>
</body>
</html>