<?php

//El namespace es el ambito del proyecto
namespace LaraDex\Http\Controllers;

//Importamos la clase Controller
use LaraDex\Http\Controllers\Controller;

/*Al igual que en java la clase debe llamarse igual que el fichero con extension .php y debe heredar de la clase Controller*/
class PrimerControlador extends Controller {

	//Definicion del controlador
	public function index() {
		return 'Primer controlador';
	}
}