<?php

namespace LaraDex\Http\Controllers;

use Illuminate\Http\Request;
use LaraDex\Trainer;

class TrainerController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $trainers = Trainer::all();

        return view('trainers.index', compact('trainers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('trainers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        //Comprobar si tenemos un archivo
        if($request->hasFile('avatar')) {
            //Almacenar y nombrar el archivo
            $archivo = $request->file('avatar');
            $nombreArchivo = time() . $archivo->getClientOriginalName();

            //Mover el archivo a la carpeta publica
            $archivo->move(public_path() . '/img/', $nombreArchivo);
        }

        $trainer = new Trainer();

        $trainer->slug = $request->input('nombre') . time();
        $trainer->nombre = $request->input('nombre');
        $trainer->avatar = $nombreArchivo;
        $trainer->save();

        return redirect()->route('trainers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer $trainer) {
        return view('trainers.show', compact('trainer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $trainer) {
        return view('trainers.edit', compact('trainer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer $trainer) {
        
        //Guardamos la ruta del avatar actual
        $image_path = public_path() .'/img/' . $trainer->avatar;
        
        //Actualizamos todos los datos masivamente exepto el avatar
        $trainer->fill($request->except('avatar'));

        if($request->hasFile('avatar')) {
            $archivo = $request->file('avatar');
            $nombreArchivo = time() . $archivo->getClientOriginalName();
            $archivo->move(public_path() . '/img/', $nombreArchivo);
            //Eliminar el avatar anterior
            unlink($image_path);
            $trainer->avatar = $nombreArchivo;
        }

        $trainer->slug = $request->input('nombre') . time();
        $trainer->save();

        return redirect()->route('trainers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
